<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2022 Gaël Bonithon <gael@xfce.org> -->
<component type="desktop-application">
  <id>org.xfce.orage</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-2.0+</project_license>
  <_name>Orage</_name>
  <_summary>Orage is a time-managing application for the Xfce desktop environment</_summary>

  <description>
    <_p>Orage aims to be a fast and easy to use graphical calendar. It uses portable ical
        format and includes common calendar features like repeating appointments and multiple
        alarming possibilities.
    </_p>
    <_p>Orage does not have group calendar features, but can only be used for single user.
        It takes a list of files for ical files that should be imported. Contents of those
        files are read and converted into Orage, but those files are left untouched.
    </_p>
  </description>

  <screenshots>
    <screenshot type="default">
      <image>https://docs.xfce.org/_media/apps/orage/orage-main.png</image>
    </screenshot>
  </screenshots>

  <url type="homepage">https://xfce.org</url>
  <url type="bugtracker">https://gitlab.xfce.org/apps/orage/</url>
  <url type="help">https://docs.xfce.org/apps/orage/start</url>
  <url type="donation">https://opencollective.com/xfce/donate</url>
  <url type="translate">https://docs.xfce.org/contribute/translate/start</url>

  <project_group>Xfce</project_group>
  <update_contact>xfce4-dev@xfce.org</update_contact>
  <developer_name>Orage Team</developer_name>
  <translation type="gettext">orage</translation>
  <launchable type="desktop-id">org.xfce.orage.desktop</launchable>
  <content_rating type="oars-1.1"/>

  <provides>
    <binary>orage</binary>
  </provides>
</component>
